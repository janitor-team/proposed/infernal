Source: infernal
Maintainer: Debian Med Packaging Team <debian-med-packaging@lists.alioth.debian.org>
Uploaders: Steffen Moeller <moeller@debian.org>,
           Andreas Tille <tille@debian.org>
Section: science
Priority: optional
Build-Depends: debhelper-compat (= 13),
               libperl4-corelibs-perl,
               python3
#Build-Depends-Indep: texlive-latex-recommended,
#                     texlive-latex-extra,
#                     texlive-fonts-recommended,
#                     rman,
#                     hmmer-examples
Standards-Version: 4.5.1
Vcs-Browser: https://salsa.debian.org/med-team/infernal
Vcs-Git: https://salsa.debian.org/med-team/infernal.git
Homepage: http://eddylab.org/infernal/
Rules-Requires-Root: no

Package: infernal
Architecture: any-amd64 any-i386
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: inference of RNA secondary structural alignments
 Infernal ("INFERence of RNA ALignment") searches DNA sequence
 databases for RNA structure and sequence similarities. It provides an
 implementation of a special variant of profile stochastic context-free
 grammars called covariance models (CMs). A CM is like a sequence
 profile, but it scores a combination of sequence consensus and RNA
 secondary structure consensus, so in many cases, it is more capable of
 identifying RNA homologs that conserve their secondary structure more
 than their primary sequence.
 .
 The tool is an integral component of the Rfam database.

Package: infernal-doc
Architecture: all
Section: doc
Multi-Arch: foreign
Depends: ${shlibs:Depends},
         ${misc:Depends}
Description: inference of RNA secondary structural alignments – documentation
 Infernal ("INFERence of RNA ALignment") searches DNA sequence
 databases for RNA structure and sequence similarities. It provides an
 implementation of a special case of profile stochastic context-free
 grammars called covariance models (CMs). A CM is like a sequence
 profile, but it scores a combination of sequence consensus and RNA
 secondary structure consensus, so in many cases, it is more capable of
 identifying RNA homologs that conserve their secondary structure more
 than their primary sequence.
 .
 This package provides the documentation which is shipped with the
 code of infernal.
