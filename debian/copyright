Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Contact: Infernal developer team at HHMI Janelia Farm:
                         Diana Kolbe <kolbed janelia.hhmi.org>
                         Eric Nawrocki <nawrockie janelia.hhmi.org>
                         Sean Eddy <eddys janelia.hhmi.org>.
Source: ftp://infernal.janelia.org/pub/software/infernal/infernal-1.1.1.tar.gz

Files: *
Copyright: © 2001-2014 HHMI Janelia Farm
           © 1991-2013      Sean R. Eddy
           © 2005-2013      Eric P. Nawrocki
           © 2005-2011      Diana L. Kolbe
           © 2004           Zasha Weinberg
           © 1990           Don G. Gilbert
           © 1995-2006      Washington University in St. Louis
           © 1992-1995      Medical Research Council, UK
           © 2004           University of Washington, Seattle
           © 1986,1993,1995 University of Toronto
           © 1989-2001      Free Software Foundation
           © 1991           Massachusetts Institute of Technology
License: GPL-3

Files: debian/*
Copyright: © 2007-2008 Steffen Moeller <moeller@debian.org>
           © 2009-2014 Andreas Tille <tille@debian.org>
License: GPL-3

Files: easel/*
Copyright: © 2004-2014 Sean R. Eddy
           © 2006-2008 Howard Hughes Medical Institute
License: BSD-Easel

Files: easel/esl_sse.c easel/esl_vmx.c
Copyright: © 2007 Julien Pommier
           © 1992 Stephen Moshier
License: BSD-Easel

Files: easel/easel.c
Copyright: © 1999-2001 David A. Wheeler
License: BSD-Easel
Comment: This only concerns esl_tmpfile()

Files: easel/esl_regexp.c
Copyright: © 1986, 1993, 1995 by University of Toronto
License: BSD-Easel

License: BSD-Easel
 @EASEL_COPYRIGHT@
 .
 The Easel library is freely modifiable and redistributable under the
 Janelia Farm Software License, a BSD license:
 .
 Redistribution and use in source and binary forms, with or without
 modification, are permitted provided that the following conditions are
 met:
 .
 1. Redistributions of source code must retain the above copyright
    notice, this list of conditions and the following disclaimer.
 .
 2. Redistributions in binary form must reproduce the above copyright
    notice, this list of conditions and the following disclaimer in
    the documentation and/or other materials provided with the
    distribution.
 .
 3. Neither the name of the Howard Hughes Medical Institute nor the
    names of its contributors may be used to endorse or promote
    products derived from this software without specific prior written
    permission.
 .
 THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 LIMITED TO, ANY IMPLIED WARRANTIES OF MERCHANTABILITY,
 NON-INFRINGEMENT, OR FITNESS FOR A PARTICULAR PURPOSE ARE
 DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS;
 REASONABLE ROYALTIES; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON
 ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR
 TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE
 USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH
 DAMAGE.


Files: Userguide.pdf
Copyright: Sean R. Eddy
License: GPL-3
Comment: The PDFs that are coming with the sources from which the userguide is
 built are indeed upstream's original sources. They are edited with a PDF
 editor. Upstream, Dr Eddy, was consulted in this issue.


License: GPL-3
 This suite of programs is free software. You can redistribute it
 and/or modify it under the terms of the GNU General Public License as
 published by the Free Software Foundation; either version 2 of the
 License, or (at your option) any later version.
 .
 In other words, you may modify, copy, or redistribute this source code
 and its documentation, but if you do, you must preserve all these
 copyrights and distribute all derivative versions as free software
 under the GNU General Public License.
 .
 This software is distributed in the hope that it will be useful, but
 WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 General Public License for more details.
 .
 You should have received a copy of the GNU General Public License
 along with this software, in the file LICENSE; if not, write to the
 Free Software Foundation, Inc., 51 Franklin St, Fifth Floor, Boston,
 MA 02110-1301, USA or see http://www.gnu.org/
Comment: On Debian systems, the complete text of the GNU General Public
 License version 3 can be found in `/usr/share/common-licenses/GPL-3'.
